Rails.application.routes.draw do
  resources :stories
  devise_for :users

  root 'library#index'
  resources :books do
    resources :collections, only: [:edit, :update, :destroy], module: "books"
    resource :merge, only: [:show, :new, :create]
  end

  resources :library, only: :index
  resources :logs, only: :index
  resources :halted_files, only: :destroy
  resources :documents, only: :show do
    get :download, on: :member
    post :convert, on: :member
    post :split, on: :member
    post :kindle, on: :member
  end
  resources :collections, only: [:index, :new, :create]
  resources :epubs, only: :show

  mount API::API => '/'
end
