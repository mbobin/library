require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_mailbox/engine"
require "action_view/railtie"
# require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Library
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.x.library_config = config_for(:library_config)
    config.active_storage.variant_processor = :vips

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.eager_load_paths << Rails.root.join("app/classes").to_s
    config.autoload_paths << Rails.root.join("app/classes").to_s

    config.active_record.schema_format = :sql
    config.enable_dependency_loading = true
    config.generators.system_tests = nil
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.default_url_options = config.x.library_config.default_url_options
    config.action_mailer.smtp_settings = config.x.library_config.smtp_settings

    config.active_job.queue_adapter = :sidekiq

    # nil will use the "default" queue
    # some of these options will not work with your Rails version
    # add/remove as necessary
    config.action_mailer.deliver_later_queue_name = nil # defaults to "mailers"
    config.active_storage.queues.analysis         = nil       # defaults to "active_storage_analysis"
    config.active_storage.queues.purge            = nil       # defaults to "active_storage_purge"
    config.active_storage.queues.mirror           = nil       # defaults to "active_storage_mirror"
    config.action_mailbox.queues.routing          = nil
    config.action_mailbox.queues.incineration     = nil

    config.hosts << config.action_mailer.default_url_options[:host]
    if ENV['LIBRARY_INTERNAL_HOST'].present?
      config.hosts << ENV['LIBRARY_INTERNAL_HOST']
    end
  end
end
