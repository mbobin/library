module Decorators
  module Document
    class Show <  BaseDecorator
      def version_type
        version.book_type.titleize
      end

      def updated_on
        updated_at.to_formatted_s(:long)
      end

      def view_in_browser_link
        return unless viewable_document?

        with_div_column do
          helpers.link_to(view_document_path, html_attributes(title: "View in browser")) do
            helpers.concat "View "
            helpers.concat octicon("eye")
          end
        end
      end

      def download_document_link
        with_div_column do
          helpers.link_to(routes.download_document_path(self), html_attributes(title: "Download")) do
            helpers.concat "Download "
            helpers.concat octicon("download")
          end
        end
      end

      def convert_document_link(current_user)
        return unless policy(current_user).convert?
        return unless convertible_document?

        with_div_column do
          helpers.content_tag(:div, class: "dropdown") do
            helpers.concat dropdown_button
            helpers.concat dropdown_conversion_items
          end
        end
      end

      def split_document_link(current_user)
        return unless policy(current_user).split?

        with_div_column do
          helpers.link_to(routes.split_document_path(self), html_attributes(title: "Create new book",method: :post)) do
            helpers.concat "Create new book "
            helpers.concat octicon("git-branch")
          end
        end
      end

      def send_to_kindle_link(current_user)
        return unless policy(current_user).kindle?
        return unless version.book_type.in?(%w[pdf mobi])

        with_div_column do
          helpers.link_to(routes.kindle_document_path(self), html_attributes(title: "Send to Kindle", method: :post)) do
            helpers.concat "Send to Kindle "
            helpers.concat octicon("paper-airplane")
          end
        end
      end

      private

      def view_document_path
        if version.pdf_books?
          routes.document_path(self)
        elsif version.epub_books?
          routes.epub_path(self)
        end
      end

      def viewable_document?
        version.book_type.in?(%(pdf epub))
      end

      def with_div_column
        helpers.content_tag(:div, class: "col-6 mt-3") do
          yield
        end
      end

      def dropdown_button
        html_attributes = {
          class: 'btn btn-secondary dropdown-toggle w-100',
          type: "button",
          id: dropdown_menu_id,
          data: { toggle: "dropdown" },
          aria: { haspopup: true, expanded: false}
        }

        helpers.content_tag(:button, html_attributes) do
          helpers.concat 'Convert to other '
          helpers.concat octicon("file")
        end
      end

      def dropdown_conversion_items
        helpers.content_tag(:div, class: 'dropdown-menu w-100', aria: { labelledby: dropdown_menu_id}) do
          available_conversion_options.each do |option|
            helpers.concat helpers.link_to("Convert to #{option.upcase}", routes.convert_document_path(self, target: option), class: "dropdown-item w-100", method: :post)
          end
        end
      end

      def available_conversion_options
        %w[pdf epub mobi] - [version.book_type]
      end

      def dropdown_menu_id
        "dropdownMenuButton-#{id}"
      end

      def convertible_document?
        !version.pdf_books?
      end

      def octicon(name)
        helpers.octicon(name, fill: "currentcolor", height: 24)
      end

      def html_attributes(title:, **attrs)
        { title: title, class: "btn btn-secondary w-100" }.merge(attrs)
      end

      def policy(user)
        DocumentPolicy.new(user, self)
      end
    end
  end
end
