module Decorators
  module Story
    class Index < BaseDecorator
      def title_link
        helpers.link_to(title_name, url, rel: :noreferrer, target: :blank)
      end

      def summarization
        description.to_s.truncate(200)
      end

      def tag_badges
        helpers.capture do
          showable_tags.each do |tag|
            helpers.concat tag_icon(tag)
          end
        end
      end

      def last_updated_at
        helpers.time_ago_in_words(updated_at)
      end

      def thumbnail
        if picture.variable?
          picture.variant(resize_to_limit: [80, nil])
        else
          picture
        end
      end

      private

      def showable_tags
        tags.to_a.take(tags_limit)
      end

      def title_name
        title
      end

      def tags_limit
        5
      end

      def tag_icon(tag)
        helpers.link_to(routes.stories_path(tag: tag), class: "mr-2") do
          helpers.content_tag(:span, tag, class: "badge badge-light")
        end
      end
    end
  end
end
