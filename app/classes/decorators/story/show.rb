module Decorators
  module Story
    class Show < Index
      private

      def title_name
        url
      end

      def tags_limit
        15
      end
    end
  end
end
