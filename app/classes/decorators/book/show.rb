module Decorators
  module Book
    class Show < Index
      def title
        name
      end

      def summarization
        helpers.capture do
          description.to_s.split("\n").each do |text|
            helpers.concat helpers.content_tag(:p, text)
          end
        end
      end

      def each_document(&block)
        documents.order(updated_at: :desc).includes(:version).each do |document|
          yield Decorators::Document::Show.new(document)
        end
      end

      private

      def showable_tags
        tags.to_a.take(15)
      end
    end
  end
end
