class PublicUrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value.present?

    PrivateNetworkGuard.enforce!(value)
  rescue PrivateNetworkGuard::BlockedUrlError => e
    record.errors.add(attribute, e.message)
  end
end
