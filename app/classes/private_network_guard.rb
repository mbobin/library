# frozen_string_literal: true

# Simplified version of:
# https://gitlab.com/gitlab-org/gitlab/blob/44afe919baad69869c16b7ea65696bc692f8f4f0/lib/gitlab/url_blocker.rb

require 'resolv'
require 'ipaddress'

class PrivateNetworkGuard
  BlockedUrlError = Class.new(StandardError)

  class << self
    def private_ip?(url)
      validate!(url, schemes: ['http', 'https'], ports: [80, 443], ascii_only: true, enforce_sanitization: false)

      false
    rescue BlockedUrlError
      true
    end

    def enforce!(url)
      validate!(url, schemes: ['http', 'https'], ports: [80, 443], ascii_only: true, enforce_sanitization: false)
    end

    # Validates the given url according to the constraints specified by arguments.
    #
    # ports - Raises error if the given URL port does is not between given ports.
    # allow_localhost - Raises error if URL resolves to a localhost IP address and argument is true.
    # ascii_only - Raises error if URL has unicode characters and argument is true.
    # enforce_sanitization - Raises error if URL includes any HTML/CSS/JS tags and argument is true.
    #
    # Returns an array with [<uri>, <original-hostname>].
    def validate!(
      url,
      ports: [],
      schemes: [],
      ascii_only: false,
      enforce_sanitization: false,
      dns_rebind_protection: true)

      return [nil, nil] if url.nil?

      # Param url can be a string, URI or Addressable::URI
      uri = parse_url(url)

      validate_uri(
        uri: uri,
        schemes: schemes,
        ports: ports,
        enforce_sanitization: enforce_sanitization,
        ascii_only: ascii_only
      )

      address_info = get_address_info(uri, dns_rebind_protection)
      return [uri, nil] unless address_info

      ip_address = ip_address(address_info)
      protected_uri_with_hostname = enforce_uri_hostname(ip_address, uri, dns_rebind_protection)

      validate_local_request(address_info: address_info)

      protected_uri_with_hostname
    end

    # Returns the given URI with IP address as hostname and the original hostname respectively
    # in an Array.
    #
    # It checks whether the resolved IP address matches with the hostname. If not, it changes
    # the hostname to the resolved IP address.
    #
    # The original hostname is used to validate the SSL, given in that scenario
    # we'll be making the request to the IP address, instead of using the hostname.
    def enforce_uri_hostname(ip_address, uri, dns_rebind_protection)
      return [uri, nil] unless dns_rebind_protection && ip_address && ip_address != uri.hostname

      new_uri = uri.dup
      new_uri.hostname = ip_address
      [new_uri, uri.hostname]
    end

    def ip_address(address_info)
      address_info.first&.ip_address
    end

    def validate_uri(uri:, schemes:, ports:, enforce_sanitization:, ascii_only:)
      validate_html_tags(uri) if enforce_sanitization

      validate_scheme(uri.scheme, schemes)
      validate_port(get_port(uri), ports) if ports.any?
      validate_hostname(uri.hostname)
      validate_unicode_restriction(uri) if ascii_only
    end

    def get_address_info(uri, dns_rebind_protection)
      Addrinfo.getaddrinfo(uri.hostname, get_port(uri), nil, :STREAM).map do |addr|
        addr.ipv6_v4mapped? ? addr.ipv6_to_ipv4 : addr
      end
    rescue SocketError
      # If the dns rebinding protection is not enabled or the domain
      # is whitelisted we avoid the dns rebinding checks
      return unless dns_rebind_protection

      # If the addr can't be resolved or the url is invalid (i.e http://1.1.1.1.1)
      # we block the url
      raise BlockedUrlError, "Host cannot be resolved or invalid"
    rescue ArgumentError => error
      # Addrinfo.getaddrinfo errors if the domain exceeds 1024 characters.
      raise unless error.message.include?('hostname too long')

      raise BlockedUrlError, "Host is too long (maximum is 1024 characters)"
    end

    def validate_local_request(address_info:)
      validate_localhost(address_info)
      validate_loopback(address_info)

      validate_local_network(address_info)
      validate_link_local(address_info)
    end

    def get_port(uri)
      uri.port || uri.default_port
    end

    def validate_html_tags(uri)
      uri_str = uri.to_s
      sanitized_uri = ActionController::Base.helpers.sanitize(uri_str, tags: [])
      if sanitized_uri != uri_str
        raise BlockedUrlError, 'HTML/CSS/JS tags are not allowed'
      end
    end

    def parse_url(url)
      raise Addressable::URI::InvalidURIError if multiline?(url)

      Addressable::URI.parse(url)
    rescue Addressable::URI::InvalidURIError, URI::InvalidURIError
      raise BlockedUrlError, 'URI is invalid'
    end

    def multiline?(url)
      CGI.unescape(url.to_s) =~ /\n|\r/
    end

    def validate_port(port, ports)
      return if port.blank?
      # Only ports under 1024 are restricted
      return if port >= 1024
      return if ports.include?(port)

      raise BlockedUrlError, "Only allowed ports are #{ports.join(', ')}, and any over 1024"
    end

    def validate_scheme(scheme, schemes)
      if scheme.blank? || (schemes.any? && !schemes.include?(scheme))
        raise BlockedUrlError, "Only allowed schemes are #{schemes.join(', ')}"
      end
    end

    def validate_hostname(value)
      return if value.blank?
      return if IPAddress.valid?(value)
      return if value =~ /\A\p{Alnum}/

      raise BlockedUrlError, "Hostname or IP address invalid"
    end

    def validate_unicode_restriction(uri)
      return if uri.to_s.ascii_only?

      raise BlockedUrlError, "URI must be ascii only #{uri.to_s.dump}"
    end

    def validate_localhost(addrs_info)
      local_ips = ["::", "0.0.0.0"]
      local_ips.concat(Socket.ip_address_list.map(&:ip_address))

      return if (local_ips & addrs_info.map(&:ip_address)).empty?

      raise BlockedUrlError, "Requests to localhost are not allowed"
    end

    def validate_loopback(addrs_info)
      return unless addrs_info.any? { |addr| addr.ipv4_loopback? || addr.ipv6_loopback? }

      raise BlockedUrlError, "Requests to loopback addresses are not allowed"
    end

    def validate_local_network(addrs_info)
      return unless addrs_info.any? { |addr| addr.ipv4_private? || addr.ipv6_sitelocal? || addr.ipv6_unique_local? }

      raise BlockedUrlError, "Requests to the local network are not allowed"
    end

    def validate_link_local(addrs_info)
      netmask = IPAddr.new('169.254.0.0/16')
      return unless addrs_info.any? { |addr| addr.ipv6_linklocal? || netmask.include?(addr.ip_address) }

      raise BlockedUrlError, "Requests to the link local network are not allowed"
    end
  end
end
