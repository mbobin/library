// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap
//= require select2/dist/js/select2
//= require jszip/dist/jszip.min.js
//= require epubjs
//= require_tree .


function renderBook(viewer) {
  var book = ePub(viewer.dataset.bookPath);

  var rendition = book.renderTo("viewer", { method: "continuous", width: "100%", height: "800" });

  var displayed = rendition.display();

  displayed.then(function(renderer){
    // console.log(renderer)
  });

  // Navigation loaded
  book.loaded.navigation.then(function(toc){
    // console.log(toc);
  });

  var keyListener = function(e){
    // Left Key
    if ((e.keyCode || e.which) == 37) {
      rendition.prev();
    }

    // Right Key
    if ((e.keyCode || e.which) == 39) {
      rendition.next();
    }
  };

  rendition.on("keyup", keyListener);
  document.addEventListener("keyup", keyListener, false);
}

document.addEventListener("turbolinks:load", function() {
  $('.select2-tags-input').select2({tags: true, tokenSeparators: [',', ';']});

  var viewer = document.getElementById('viewer');

  if(viewer) {
    renderBook(viewer);
  }

  $('.ac-select2').each(function() {
    var url = $(this).data('url');

    $(this).select2({
      minimumInputLength: 2,
      multiple: false,
      ajax: {
        url: url,
        dataType: 'json'
      }
    });
  });
});
