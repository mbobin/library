class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.config.x.library_config.dig(:smtp_settings, :user_name)
  layout 'mailer'
end
