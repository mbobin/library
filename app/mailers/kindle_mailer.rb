class KindleMailer < ApplicationMailer
  def send_file
    file = params[:document].file

    attachments[file.filename.to_s] = {
      mime_type: file.content_type,
      content: file.download
    }

    mail(to: params[:user].kindle_email, subject: "")
  end
end
