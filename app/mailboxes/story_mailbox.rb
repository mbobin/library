class StoryMailbox < ApplicationMailbox
  def process
    return unless user
    return unless text_part

    urls = text_part.decoded.split.map(&:strip).select(&:present?)

    urls.each do |url|
      StoryUrl.new(url: url, user: user).save
    end
  end

  def user
    @user ||= User.find_by(subscription_token: subscription_token)
  end

  def text_part
    @text_part ||= mail.body.parts.find { |part| part.content_type.include?('text/plain') }
  end

  def subscription_token
    @subscription_token ||= mail.to.first.split('@').first.split('+').last
  end
end
