class BooksController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy]

  def index
    @books = Book
      .page(params[:page]).per(26)
      .includes(:authors)
    @books = @books.with_tag(params[:tag]) if params[:tag].present?
    @books = @books.search(params[:q]) if params[:q].present?
    @books = @books.order(created_at: :desc) unless @books.order_values.any?
    @books = Decorators::Book::Index.pagination_wrap(@books)
  end

  def new
    authorize Book
  end

  def create
    authorize Book

    if params[:books].present?
      params[:books].each { |book| Book.create_from_tempfile(book.tempfile) }

      redirect_to books_path, notice: 'Adding it to the library'
    else
      render :new
    end
  end

  def show
    @book = Book.find(params[:id])
    @book = Decorators::Book::Show.new(@book)
  end

  def edit
    @book = Book.find(params[:id])
    authorize @book
  end

  def update
    @book = Book.find(params[:id])
    authorize @book

    if @book.update(book_params)
      PipelineUpdateJob.perform_later(@book.id, @book.isbn) if @book.update_from_isbn
      redirect_to @book, notice: "Book will be updated"
    else
      render :edit
    end
  end

  def destroy
    @book = Book.find(params[:id])
    authorize @book

    @book.destroy_everything
    redirect_to books_path, notice: 'Book was successfully destroyed.'
  end

  private
    def book_params
      params
        .require(:book)
        .permit(:isbn, :name, :description, :update_from_isbn, tags: [])
        .tap do |prms|
          prms[:tags] = prms[:tags].select(&:present?)
          prms[:update_from_isbn] = string_to_bool(prms[:update_from_isbn])
        end
    end

    def string_to_bool(value)
      ActiveRecord::Type::Boolean.new.cast(value)
    end
end
