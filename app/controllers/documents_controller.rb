class DocumentsController < ApplicationController
  def index
    @books ||= Book.all.includes(:authors)
  end

  def show
    file = document.file

    send_data file.download, type: file.content_type, disposition: :inline
  end

  def download
    file = document.file

    send_data file.download, type: file.content_type,
      disposition: :attachment,
      filename: "#{document.book.name}.#{document.version.book_type}"
  end

  def convert
    authorize document

    PipelineConvertJob.perform_later(document, params[:target] || 'pdf')
    redirect_to document.book, notice: "Conversion enqueued"
  end

  def split
    authorize document
    document.split!

    redirect_to document.book
  end

  def kindle
    authorize document

    KindleMailer
      .with(document: document, user: current_user)
      .send_file
      .deliver_later

    redirect_to document.book, notice: "The file will be sent to your Kindle device"
  end

  private

  def document
    @document ||= Document.find(params[:id])
  end
end
