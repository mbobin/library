class StoriesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :set_story, only: [:show, :edit, :update, :destroy]

  def index
    @stories = Story.all
      .with_attached_picture
      .with_attached_favicon

    @stories = @stories.with_tag(params[:tag]) if params[:tag].present?
    @stories = @stories.search(params[:q]) if params[:q].present?
    @stories = @stories.order(updated_at: :desc) unless @stories.order_values.any?
    @stories = @stories.page(params[:page]).per(26)

    @stories = Decorators::Story::Index.pagination_wrap(@stories)
  end

  def show
    @story = Decorators::Story::Show.new(@story)
  end

  def new
    @story = Story.new(url: params[:url])
  end

  def edit
  end

  def create
    @story = StoryUrl.new(create_params)

    if @story.save
      redirect_to stories_url, notice: 'Story was successfully enqueued for creation'
    else
      render :new
    end
  end

  def update
    if @story.update(update_params)
      redirect_to @story, notice: 'Story was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @story.destroy
    redirect_to stories_url, notice: 'Story was successfully destroyed.'
  end

  private

    def set_story
      @story = Story.find(params[:id])
    end

    def create_params
      params.require(:story).permit(:url).merge(user: current_user)
    end

    def update_params
      params.require(:story).permit(:title, :description, tags: []).tap do |prms|
        prms[:tags].select!(&:present?)
      end
    end
end
