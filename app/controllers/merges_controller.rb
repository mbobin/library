class MergesController < ApplicationController
  before_action :authenticate_user!

  def new
    authorize book
  end

  def create
    authorize book

    book.merge!(Book.find(params[:duplicate_book_id]))

    redirect_to book_path(book), notice: "Books merged"
  end

  def show
    books = Book.select(:id, :name)
      .where.not(id: params[:book_id])
      .search(params[:term])
      .limit(10)

    respond_to do |format|
      format.json { render json: { results: books.map { |b| { id: b.id, text: b.name } } } }
    end
  end

  private

  def book
    @book ||= Book.find(params[:book_id])
  end
end
