class UnfurlJob < ApplicationJob
  queue_as :default

  attr_reader :page, :url, :user_id

  def perform(user_id, url)
    @user_id = user_id
    @url = url
    @page = MetaInspector.new(url, unfurl_options)

    call
  end

  def call
    if page.response.success?
      save_story
    elsif redirected?
      handle_redirect
    end
  end

  def save_story
    attributes = {
      url: page.untracked_url,
      domain: page.host,
      title: page.best_title,
      description: page.best_description.to_s.strip,
      website_icon_url: page.images.favicon,
      image_url: page.images.best,
      tags: extract_tags(page),
      author_id: user_id
    }

    story = Story.create!(attributes)
    AttachRemoteImageJob.perform_later(story, :favicon, story.website_icon_url)
    AttachRemoteImageJob.perform_later(story, :picture, story.image_url)
  end

  def handle_redirect
    return unless scheme_change?

    UnfurlJob.perform_later(user_id, redirect_location)
  end

  def scheme_change?
    [url, redirect_location]
      .map(&method(:parse_location))
      .reduce(&:eql?)
  end

  def redirected?
    page.response.status.in?(300..399)
  end

  def parse_location(uri)
    Addressable::URI.parse(uri).normalize.to_hash.except(:scheme)
  end

  def redirect_location
    page.response.headers['location']
  end

  def unfurl_options
    {
      encoding: 'UTF-8',
      connection_timeout: 10,
      read_timeout: 5,
      allow_redirections: false,
      download_images: false
    }
  end

  def extract_tags(page)
    page
      .meta_tags
      .dig('name','keywords')
      .to_a
      .map { |tag| tag.split(',').map(&:strip) }
      .flatten
      .compact
  end
end
