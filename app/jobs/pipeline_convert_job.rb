class PipelineConvertJob < ApplicationJob
  def perform(document, target_format)
    Pipelines::Convert.execute(document, target_format)
  end
end
