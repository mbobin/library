class ExecutePipelineJob < ApplicationJob
  def perform(pipeline)
    pipeline.running!
    pipeline.execute
    pipeline.completed! unless pipeline.waiting? || pipeline.failed?
  end
end
