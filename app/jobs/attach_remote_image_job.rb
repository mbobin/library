# Methods from:
# https://github.com/carrierwaveuploader/carrierwave/blob/9a37fc9e7ce2937c66d5419ce1943ed114385beb/lib/carrierwave/downloader/remote_file.rb
#
class AttachRemoteImageJob < ApplicationJob
  queue_as :default

  attr_reader :tempfile

  def perform(story, attribute, url)
    return unless url.present?

    @tempfile = OpenURI.open_uri(URI.parse(url))
    return if tempfile.size > 5.megabytes

    story.public_send(attribute).attach(io: tempfile, filename: filename)
  ensure
    @tempfile.try(:unlink)
  end

  def filename
    name = filename_from_header || filename_from_uri
    mime_type = MiniMime.lookup_by_content_type(tempfile.content_type)
    unless File.extname(name).present? || mime_type.blank?
      name = "#{name}.#{mime_type.extension}"
    end
    name
  end

  def filename_from_header
    return nil unless tempfile.meta.include? 'content-disposition'

    match = tempfile.meta['content-disposition'].match(/filename=(?:"([^"]+)"|([^";]+))/)
    return nil unless match

    match[1].presence || match[2].presence
  end

  def filename_from_uri
    CGI.unescape(File.basename(tempfile.base_uri.path))
  end
end
