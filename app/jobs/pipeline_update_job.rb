class PipelineUpdateJob < ApplicationJob
  def perform(book_id, isbn)
    Pipelines::Update.execute(book_id, isbn)
  end
end
