# frozen_string_literal: true
require 'grape_logging'

module API
  class API < Grape::API
    logger.formatter = GrapeLogging::Formatters::Default.new
    use GrapeLogging::Middleware::RequestLogger, { logger: logger }

    prefix 'api'
    version 'v1'
    format :json

    helpers do
      def verify_basic_auth(username, password)
        verify_user = ActiveSupport::SecurityUtils
          .secure_compare(auth_config[:username], username)

        verify_password = ActiveSupport::SecurityUtils
          .secure_compare(auth_config[:password], password)

        verify_user && verify_password
      end

      def auth_config
        Rails.application.config.x.library_config.api_auth
      end
    end

    http_basic do |username, password|
      verify_basic_auth(username, password)
    end

    mount Calibre::Operations
  end
end
