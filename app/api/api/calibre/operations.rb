# frozen_string_literal: true

module API
  module Calibre
    class Operations < Grape::API
      namespace '/calibre/operations' do
        params do
          optional :limit, type: Integer, default: 5
        end
        post '/request' do
          service = RegisterOperationsService.new(params[:limit])
          operations = service.execute

          status :ok
          present operations, with: Entities::CalibreOperation
        end

        params do
          requires :status, type: String
          requires :id, type: String
        end
        post '/:id/update' do
          operation = CalibreOperation.find(params[:id])
          operation.update(status: params[:status], finished_at: Time.current)
          if operation.success?
            operation.pipeline.pending! if operation.pipeline.waiting?
            operation.pipeline.execute_async if operation.pipeline.pending?
          end
          status :ok
          operation.errors
        end

        params do
          requires :id, type: Integer
          optional :metadata, type: Hash do
            optional :title, type: String
            optional :description, type: String
            optional :published_at, type: Date
            optional :isbn, type: String
            optional :authors, type: Array
            optional :tags, type: Array
          end
        end
        post '/:id/metadata' do
          operation = CalibreOperation.find(params[:id])
          operation.update(result: declared(params).slice(:metadata))
          status :ok
          operation.errors
        end

        params do
          requires :id, type: Integer
          requires :document, type: File
        end
        post '/:id/upload' do
          operation = CalibreOperation.find(params[:id])
          file = params[:document][:tempfile]

          destination = Pathname(Rails.application.config.x.library_config[:upload_path])
            .join(Pathname(file).basename.to_s)
            .expand_path

          FileUtils.cp(file, destination)

          operation.update(result: { converted_file_path: destination })
          status :ok
          operation.errors
        end
      end
    end
  end
end
