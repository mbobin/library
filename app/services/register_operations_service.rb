class RegisterOperationsService
  def initialize(limit)
    @limit = limit
  end

  def execute
    operations = []

    CalibreOperation.transaction do
      operations = load_pending_operations
      register_operations(operations)
    end

    operations
  end

  def load_pending_operations
    CalibreOperation
      .pending.order(id: :asc)
      .limit(@limit)
      .lock('for update skip locked')
      .load
  end

  def register_operations(operations)
    return unless operations.any?

    CalibreOperation
      .where(id: operations.ids)
      .update_all(status: :running, started_at: Time.current)
  end
end
