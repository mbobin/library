module Pipelines
  class Create
    def self.execute(path)
      pipeline = Pipeline.create!(
        operation_type: :create,
        current_step_idx: 0,
        payload: { file_path: path }
      )
      pipeline.execute_async
      pipeline
    end

    def self.steps
      @steps ||= [
        Steps::Initializer,
        Steps::Checksum,
        Steps::CreateTempDocument,
        Steps::RequestFileMetadata,
        Steps::PopulateFileMetadata,
        Steps::ExtractIsbnFromFile,
        Steps::RequestMetadataForIsbn,
        Steps::PopulateMetadataForIsbn,
        Steps::FindMetadataFromGoogleBooks,
        Steps::FindMetadataFromJustBooks,
        Steps::PersistData,
        Steps::RemoveFile
      ]
    end
  end
end
