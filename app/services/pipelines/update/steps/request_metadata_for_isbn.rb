module Pipelines
  class Update
    module Steps
      class RequestMetadataForIsbn < Base
        def perform
          return unless isbn.present?

          pipeline.transaction do
            operation = pipeline.calibre_operations.create!(status: :pending, payload: operation_payload)
            pipeline.waiting_calibre_operation = operation
            pipeline.status = :waiting
          end
        end

        def isbn
          payload.dig('input', 'isbn')
        end

        def operation_payload
          { operation: :extract_metadata,
            options: {
              source: :isbn,
              isbn: isbn
            }
          }
        end
      end
    end
  end
end
