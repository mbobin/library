module Pipelines
  class Update
    module Steps
      class PersistData < Base
        def perform
          pipeline.transaction do
            authors  = find_or_create_authors
            update_book(authors)
          end
        end

        private

        def author_names
          payload.dig('metadata', 'authors').presence || ["Unknown"]
        end

        def find_or_create_authors
          author_names.map { |name| Author.find_or_create_by!(name: name) }
        end

        def update_book(authors)
          attributes = {
            authors: authors,
            name: payload.dig('metadata', 'title'),
            description: payload.dig('metadata', 'description'),
            tags: payload.dig('metadata', 'tags'),
          }.compact

          Book
            .find(payload.dig('input', 'book_id'))
            .update!(attributes)
        end
      end
    end
  end
end
