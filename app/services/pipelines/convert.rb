module Pipelines
  class Convert
    def self.execute(document, target_format)
      pipeline = Pipeline.create!(
        operation_type: :convert,
        current_step_idx: 0,
        payload: {
          input: {
            document_id: document.id,
            target_format: target_format
          }
        }
      )
      pipeline.execute_async
      pipeline
    end

    def self.steps
      @steps ||= [
        Steps::Validate,
        Steps::RequestFileConversion,
        Steps::ProcessConvertedFile,
        Steps::Checksum,
        Steps::PersistData,
        Steps::RemoveFile
      ]
    end
  end
end

