module Pipelines
  class Convert
    module Steps
      class RequestFileConversion < Base
        def perform
          pipeline.transaction do
            operation = CalibreOperation.create!(pipeline: pipeline, status: :pending, payload: operation_payload)
            pipeline.status = :waiting
            set_attribute('pending_calibre_operation_id', operation.id)
          end
        end

        def document
          @document ||= Document.find(payload.dig('input', 'document_id'))
        end

        def file_url
          Rails.application.routes.url_helpers.rails_blob_url(document.file, only_path: true)
        end

        def operation_payload
          { operation: :convert,
            options: {
              file_url: file_url,
              target_format: payload.dig('input', 'target_format')
            }
          }
        end
      end
    end
  end
end
