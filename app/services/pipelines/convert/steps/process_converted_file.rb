module Pipelines
  class Convert
    module Steps
      class ProcessConvertedFile < Base
        def perform
          operation = CalibreOperation.find_by_id(payload['pending_calibre_operation_id'])
          return unless operation

          set_attribute('file_path', operation.result['converted_file_path'])
        end
      end
    end
  end
end
