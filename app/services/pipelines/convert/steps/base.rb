module Pipelines
  class Convert
    module Steps
      class Base
        def initialize(pipeline)
          @pipeline = pipeline
        end

        def self.execute(pipeline)
          new(pipeline).execute
        end

        def execute
          perform
          continue! unless pipeline.failed?
        end

        private

        attr_reader :pipeline
        delegate :payload, to: :pipeline

        def continue!
          pipeline.increment(:current_step_idx)
          yield if block_given?

          pipeline.save!
        end

        def drop!(reason)
          pipeline.status = :failed
          set_attribute('failed_reason', reason)
          pipeline.save!
        end

        def set_attribute(name, value)
          pipeline.payload.deep_merge!(name => value)
        end
      end
    end
  end
end
