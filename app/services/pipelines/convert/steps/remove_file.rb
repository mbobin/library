module Pipelines
  class Convert
    module Steps
      class RemoveFile < Base
        def perform
          remove_file if file_exists? && trash_path_exists?
          remove_calibre_operations
        end

        def remove_file
          FileUtils.mv(payload["file_path"].to_s, trash_path)
        end

        def remove_calibre_operations
          pipeline.calibre_operations.destroy_all
        end

        def file_exists?
          File.exists?(payload["file_path"].to_s)
        end

        def trash_path
          Rails.application.config.x.library_config[:trash_path]
        end

        def trash_path_exists?
          File.exists?(trash_path.to_s)
        end
      end
    end
  end
end
