module Pipelines
  class Convert
    module Steps
      class Validate < Base
        ALLOWED_FORMATS = %w[pdf epub mobi]

        def perform
          drop!('Unsupported format') unless ALLOWED_FORMATS.include?(target_format)
          drop!('Can not convert to same format') if target_format == version.book_type
        end

        def version
          @version ||= Version
            .joins(:documents)
            .where(documents: { id: payload.dig('input', 'document_id') })
            .first!
        end

        def target_format
          payload.dig('input', 'target_format')
        end
      end
    end
  end
end
