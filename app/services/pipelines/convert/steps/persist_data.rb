module Pipelines
  class Convert
    module Steps
      class PersistData < Base
        def perform
          converted_document = nil

          pipeline.transaction do
            book     = find_book
            version  = create_version(book)
            converted_document = create_document(version)
          end

          converted_document
            .file
            .attach(io: File.open(payload["file_path"]),
              filename: "#{SecureRandom.uuid}.#{book_type}")
        end

        private

        def find_book
          Document.find(payload.dig('input', 'document_id')).book
        end

        def create_version(book)
          attributes = {
            book: book,
            book_type: book_type
          }

          Version.create!(attributes)
        end

        def book_type
          payload.dig('input', 'target_format')
        end

        def create_document(version)
          attributes = {
            version: version,
            sha_hash: payload["file_digest"],
          }

          Document.create!(attributes)
        end
      end
    end
  end
end
