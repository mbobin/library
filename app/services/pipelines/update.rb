module Pipelines
  class Update
    def self.execute(book_id, isbn)
      pipeline = Pipeline.create!(
        operation_type: :update,
        current_step_idx: 0,
        payload: {
          input: {
            book_id: book_id,
            isbn: isbn
          }
        }
      )
      pipeline.execute_async
      pipeline
    end

    def self.steps
      @steps ||= [
        Steps::RequestMetadataForIsbn,
        Steps::PopulateMetadataForIsbn,
        Steps::FindMetadataFromGoogleBooks,
        Steps::FindMetadataFromJustBooks,
        Steps::PersistData,
      ]
    end
  end
end

