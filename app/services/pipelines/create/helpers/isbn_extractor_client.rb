module Pipelines
  class Create
    module Helpers
      class IsbnExtractorClient
        class IsbnError < StandardError; end

        def execute(file_path)
          payload = { book: Faraday::FilePart.new(file_path, mime_type(file_path)) }
          response = faraday.post('/api/v1/extract', payload)
          parse(response)
        end

        private

        def faraday
          @faraday ||= Faraday.new(api_url) do |f|
            f.request :multipart
            f.request :url_encoded
            f.adapter :net_http
          end
        end

        def parse(response)
          raise IsbnError, response.body unless response.success?

          data = JSON.parse(response.body)
          data = ActionController::Parameters.new(data)
          data.permit(:isbn).to_h
        end

        def mime_type(file_path)
          Marcel::MimeType.for(identifiable_chunk(file_path), name: file_path)
        end

        def identifiable_chunk(file_path)
          File.read(file_path, 4.kilobytes)
        end

        def api_url
          Rails.application.config.x.library_config[:isbn_extractor_api]
        end
      end
    end
  end
end
