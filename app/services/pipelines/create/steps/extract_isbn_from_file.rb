module Pipelines
  class Create
    module Steps
      class ExtractIsbnFromFile < Base
        def perform
          return if payload.dig('metadata', 'isbn')

          isbn_data = extract_isbn.compact
          payload.deep_merge!('metadata' => isbn_data) if isbn_data['isbn'].present?
        end

        def extract_isbn
          Helpers::IsbnExtractorClient.new.execute(payload['file_path'])
        end
      end
    end
  end
end
