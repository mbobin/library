module Pipelines
  class Create
    module Steps
      class PopulateMetadataForIsbn < Base
        def perform
          operation = pipeline.waiting_calibre_operation
          return unless operation

          merge_data(operation.result['metadata'])
        end

        def merge_data(data)
          new_description = data.delete('description')
          new_tags = data.delete('tags')

          payload['metadata']['tags'] = Array(payload.dig('metadata','tags')).concat(new_tags).uniq
          payload['metadata']['description'] = new_description unless payload.dig('metadata','description').to_s.size > new_description.to_s.size
          payload['metadata'].merge!(data.compact)
        end
      end
    end
  end
end
