module Pipelines
  class Create
    module Steps
      class CreateTempDocument < Base
        def perform
          temp_doc = pipeline.temp_documents.new
          temp_doc.file.attach(io: File.open(payload['file_path']), filename: File.basename(payload['file_path']))
          temp_doc.save!
          set_attribute('temp_doc_id', temp_doc.id)
        end
      end
    end
  end
end
