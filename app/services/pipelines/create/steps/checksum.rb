module Pipelines
  class Create
    module Steps
      class Checksum < Base
        def perform
          set_attribute('file_digest', file_digest)

          drop!("Document already exists") if document_exists?
        end

        def file_digest
          @file_digest ||= Digest::SHA256.file(pipeline.payload['file_path']).hexdigest
        end

        def document_exists?
          Document.where(sha_hash: file_digest).exists?
        end
      end
    end
  end
end
