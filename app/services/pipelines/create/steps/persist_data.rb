module Pipelines
  class Create
    module Steps
      class PersistData < Base
        def perform
          fetch_book_type
          document = nil

          pipeline.transaction do
            authors  = find_or_create_authors
            book     = find_or_create_book(authors)
            version  = create_version(book)
            document = create_document(version)
          end

          document
            .file
            .attach(io: File.open(payload["file_path"]),
              filename: "#{SecureRandom.uuid}.#{book_type}")
        end

        private

        attr_reader :book_type

        def author_names
          payload.dig('metadata', 'authors').presence || ["Unknown"]
        end

        def find_or_create_authors
          author_names.map { |name| Author.find_or_create_by!(name: name) }
        end

        def find_or_create_book(authors)
          find_book_by_isbn || find_book_by_name || create_book(authors)
        end

        def find_book_by_isbn
          isbn = payload.dig('metadata', 'isbn')

          if isbn.present?
            Book.find_by(isbn: isbn)
          end
        end

        def find_book_by_name
          name = payload.dig('metadata', 'title')

          if name.present?
            Book.search_by_name(name).first
          end
        end

        def create_book(authors)
          attributes = {
            authors: authors,
            name: payload.dig('metadata', 'title'),
            description: payload.dig('metadata', 'description'),
            tags: payload.dig('metadata', 'tags').to_a,
            isbn: payload.dig('metadata', 'isbn'),
          }

          Book.create!(attributes)
        end

        def create_version(book)
          attributes = {
            book: book,
            book_type: book_type,
            published_at: payload.dig('metadata', 'published_at'),
          }

          Version.create!(attributes)
        end

        def fetch_book_type
          @book_type ||= begin
            long_type = Marcel::MimeType.for(identifiable_chunk, name: payload['file_path'])
            Mime::Type.lookup(long_type).symbol
          end
        end

        def identifiable_chunk
          File.read(payload["file_path"], 4.kilobytes)
        end

        def create_document(version)
          attributes = {
            version: version,
            sha_hash: payload["file_digest"],
          }

          Document.create!(attributes)
        end
      end
    end
  end
end
