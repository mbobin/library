module Pipelines
  class Create
    module Steps
      class PopulateFileMetadata < Base
        def perform
          operation = pipeline.waiting_calibre_operation
          return unless operation

          metadata = operation.result['metadata'].to_h.compact
          set_attribute('metadata', metadata)
        end
      end
    end
  end
end
