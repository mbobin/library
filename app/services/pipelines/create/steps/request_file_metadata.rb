module Pipelines
  class Create
    module Steps
      class RequestFileMetadata < Base
        def perform
          pipeline.transaction do
            operation = pipeline.calibre_operations.create!(status: :pending, payload: operation_payload)
            pipeline.waiting_calibre_operation = operation
            pipeline.status = :waiting
          end
        end

        def temp_doc
          @temp_doc ||= TempDocument.find(payload['temp_doc_id'])
        end

        def file_url
          Rails.application.routes.url_helpers.rails_blob_url(temp_doc.file, only_path: true)
        end

        def operation_payload
          { operation: :extract_metadata,
            options: {
              source: :file,
              file_url: file_url,
            }
          }
        end
      end
    end
  end
end
