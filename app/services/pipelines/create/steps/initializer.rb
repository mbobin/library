module Pipelines
  class Create
    module Steps
      class Initializer < Base
        def perform
          unless File.exists?(payload['file_path'].to_s)
            drop!("File not found")
          end
        end
      end
    end
  end
end
