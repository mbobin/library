require 'google/apis/books_v1'

module Pipelines
  class Create
    module Steps
      class FindMetadataFromGoogleBooks < Base
        def perform
          return unless isbn
          return if data_already_present?

          merge_data if book
        end

        def data_already_present?
          payload['metadata']
            .values_at('title', 'authors', 'description', 'published_at', 'tags')
            .all?(&:present?)
        end

        def isbn
          payload.dig('metadata', 'isbn')
        end

        def book
          @book ||= search_books
        end

        def search_books
          service = Google::Apis::BooksV1::BooksService.new
          data = service.list_volumes(q: "isbn:#{isbn}", max_results: 1)
          data.items&.first&.volume_info
        end

        def merge_data
          payload['metadata']['title'] ||= [book.title, book.subtitle].compact.join(": ")
          payload['metadata']['authors'] = book.authors if payload.dig('metadata', 'authors').to_a.empty?
          payload['metadata']['description'] ||= book.description.presence
          payload['metadata']['published_at'] ||= published_date
          payload['metadata']['tags'] = Array(payload.dig('metadata', 'tags')).concat(book.categories.to_a).uniq
        end

        def published_date
          Date.strptime(book.published_date, "%Y-%m") rescue nil
        end
      end
    end
  end
end
