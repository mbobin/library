module Pipelines
  class Create
    module Steps
      class RemoveFile < Base
        def perform
          remove_file if file_exists? && trash_path_exists?
          remove_temp_document
          remove_calibre_operations
        end

        def remove_file
          FileUtils.mv(payload["file_path"].to_s, trash_path)
        end

        def remove_temp_document
          TempDocument.where(id: payload['temp_doc_id']).destroy_all
        end

        def remove_calibre_operations
          pipeline.waiting_calibre_operation = nil
          pipeline.calibre_operations.destroy_all
        end

        def file_exists?
          File.exists?(payload["file_path"].to_s)
        end

        def trash_path
          Rails.application.config.x.library_config[:trash_path]
        end

        def trash_path_exists?
          File.exists?(trash_path.to_s)
        end
      end
    end
  end
end
