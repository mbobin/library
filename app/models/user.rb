class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :recoverable, #:registerable,
  devise :database_authenticatable, :rememberable, :trackable, :validatable
  has_many :collections
  enum role: {
    regular: 1,
    admin: 10,
  }

  before_validation :assign_subscription_token

  def subscription_email
    email = ENV.fetch('EMAIL_ADDRESS')
    email.split('@').join("+#{subscription_token}@")
  end

  private

  def assign_subscription_token
    self.subscription_token ||= SecureRandom.hex
  end
end
