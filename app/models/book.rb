class Book < ApplicationRecord
  include PgSearch::Model
  attr_accessor :update_from_isbn

  has_and_belongs_to_many :authors
  has_and_belongs_to_many :collections
  has_many :versions, dependent: :destroy
  has_many :documents, through: :versions

  scope :with_tag, ->(tag) {
    where("? = ANY(tags)", tag)
  }

  pg_search_scope :search_by_name,
    against: :name,
    using: {
      trigram: { threshold: 0.85 }
    }

  pg_search_scope :search, against: [
      [:name, 'A'],
      [:tags, 'A'],
      [:description, 'B'],
    ],
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'english',
        tsvector_column: 'searchable'
      }
    }

  def copy_versions_from(other)
    self.versions << other.versions
  end

  def merge!(other)
    self.transaction do
      self.copy_versions_from(other)
      other.reload
      other.destroy!
    end
  end

  def destroy_everything
    ImportLog.where("data->'book'->>'id' = ?", self.id.to_s).find_each do |log|
      delete_backup(log)
      log.destroy
    end

    self.destroy
  end

  def backup_path
    Rails.application.config.x.library_config[:trash_path]
  end

  def delete_backup(log)
    return unless File.exists?(backup_path.to_s)
    return unless log.data['file_path'].present?

    filename = File.basename(log.data['file_path'])
    backup = Pathname(backup_path + "/" + filename).expand_path
    backup.unlink if backup.exist?
  end

  def self.create_from_tempfile(file)
    return unless Pathname(file).exist?

    destination = Pathname(Rails.application.config.x.library_config[:upload_path])
      .join(Pathname(file).basename.to_s)
      .expand_path

    FileUtils.cp(file, destination)
    Pipelines::Create.execute(destination.to_s)
  end
end
