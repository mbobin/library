class CalibreOperation < ApplicationRecord
  belongs_to :pipeline

  enum status: {
    created: 0,
    pending: 1,
    running: 2,
    success: 3,
    failed: 4
  }
end
