class Pipeline < ApplicationRecord
  has_many :temp_documents, dependent: :destroy
  has_many :calibre_operations, dependent: :destroy
  belongs_to :waiting_calibre_operation, class_name: 'CalibreOperation', optional: true

  enum operation_type: {
    create: 0,
    convert: 1,
    update: 2
  }, _suffix: :flow

  enum status: {
    pending: 0,
    running: 1,
    waiting: 2,
    completed: 3,
    failed: 4
  }

  def execute
    steps.from(current_step_idx).each do |step|
      step.execute(self)
      break if waiting? || failed?
    end
  end

  def execute_async
    ExecutePipelineJob.perform_later(self)
  end

  def steps
    "Pipelines::#{operation_type.classify}".constantize.steps
  end
end
