class StoryUrl
  include ActiveModel::Validations
  attr_accessor :url, :user

  validates :url, presence: true, length: { maximum: 1024 }
  validates :user, presence: true
  validates :url, public_url: true

  def initialize(attrs={})
    @url = attrs[:url]
    @user = attrs[:user]
  end

  def save
    valid? && enqueue
  end

  def to_model
    Story.new(url: url, author_id: user&.id)
  end

  def persisted?
    errors.empty?
  end

  private

  def enqueue
    UnfurlJob.perform_later(user.id, url)
  end
end
