class TempDocument < ApplicationRecord
  belongs_to :pipeline
  has_one_attached :file
end
