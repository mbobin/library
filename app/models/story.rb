class Story < ApplicationRecord
  include PgSearch::Model

  has_one_attached :favicon
  has_one_attached :picture

  pg_search_scope :search, against: [
      [:title, 'A'],
      [:tags, 'A'],
      [:description, 'B'],
      [:domain, 'C'],
    ],
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'english',
        tsvector_column: 'searchable'
      }
    }

  scope :with_tag, ->(tag) {
    where('? = ANY(tags)', tag)
  }
end
