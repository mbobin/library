class Document < ApplicationRecord
  belongs_to :version
  has_one :book, through: :version
  has_one_attached :file

  def split!
    self.transaction do
      book = Book.create!(name: "Split of #{self.book.name}")
      self.version.dup.tap do |ver|
        ver.book_id = book.id
        ver.documents << self
      end.save!
    end
  end
end
