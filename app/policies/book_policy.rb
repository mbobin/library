class BookPolicy < ApplicationPolicy
  def index?
    true
  end

  def update?
    user&.admin?
  end

  def destroy?
    update?
  end

  def new?
    update?
  end

  def create?
    update?
  end
end
