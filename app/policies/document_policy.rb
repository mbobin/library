class DocumentPolicy < ApplicationPolicy
  def convert?
    user&.admin?
  end

  def split?
    convert?
  end

  def kindle?
    !!user
  end
end
