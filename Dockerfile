FROM ruby:2.7-alpine as builder

RUN apk add --no-cache --update \
  build-base \
  linux-headers \
  postgresql-dev \
  tzdata \
  yarn \
  makepasswd

ENV APP_PATH /var/apps/library
ENV RAILS_ENV production

WORKDIR $APP_PATH
ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
RUN bundle install \
  --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` \
  --retry 3 \
  --without test development \
  --deployment

COPY . $APP_PATH
RUN bin/stub_env bin/rake assets:precompile \
  && rm -rf node_modules

FROM ruby:2.7-alpine

RUN apk add --no-cache --update \
  postgresql-dev \
  tzdata \
  vips \
  nodejs

ENV APP_PATH /var/apps/library
ENV RAILS_ENV production
WORKDIR $APP_PATH

COPY --from=builder $APP_PATH $APP_PATH

RUN bundle install \
  --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` \
  --retry 3 \
  --without test development \
  --local \
  --deployment \
  --frozen \
  --clean

EXPOSE 3000

CMD ["bin/server"]
