# Library

Useful for managing your library of books and bookmarks.

## Running with docker-compose

create a `docker-compose.yml` file with:

```yml
version: '3.5'

services:
  database:
    image: postgres:12-alpine
    env_file:
      - .db.env
    restart: always
    volumes:
     - database-storage:/var/lib/postgresql/data

  library-web:
    image: registry.gitlab.com/mbobin/library/web
    command: bin/server
    env_file:
      - .prod.env
    depends_on:
      - database
    ports:
      - '3000:3000'
    restart: always
    volumes:
      - files-storage:/var/apps/library/storage
      - files-upload:/var/apps/library/upload
      - files-trash:/var/apps/library/trash

  library-worker:
    image: registry.gitlab.com/mbobin/library/worker
    command: bin/rake jobs:work
    env_file:
      - .prod.env
    depends_on:
     - database
    restart: always
    volumes:
      - files-storage:/var/apps/library/storage
      - files-upload:/var/apps/library/upload
      - files-trash:/var/apps/library/trash

  # Used to submit stories by email
  library-relay:
    image: registry.gitlab.com/mbobin/library_relay
    env_file:
      - .relay.env
    depends_on:
      - library-web
    restart: always
    volumes:
      - relay-logs:/var/apps/relay/logs

volumes:
  database-storage:
  files-storage:
  files-upload:
  files-trash:
  relay-logs:
```

And the `.env` files:

### .db.env

```
POSTGRES_PASSWORD=...
POSTGRES_USER=librarian
POSTGRES_DB=library_production
```

### .prod.env

```
RAILS_SERVE_STATIC_FILES=true
RAILS_LOG_TO_STDOUT=true
DATABASE_URL=postgres://librarian:POSTGRES_PASSWORD@database/library_production
SECRET_KEY_BASE=...
RAILS_INBOUND_EMAIL_PASSWORD=...
EMAIL_ADDRESS=...
```

### .relay.env

```
EMAIL_ADDRESS=...
EMAIL_PASSWORD=...
APP_URL=http://library-web:3000
POSTBACK_PASSWORD=.... # same as RAILS_INBOUND_EMAIL_PASSWORD
```
## Starting up

```
docker-compose pull
docker-compose up -d
docker-compose run --rm library-web bin/rake db:migrate
```
