class AddKindleEmailToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :kindle_email, :string
  end
end
