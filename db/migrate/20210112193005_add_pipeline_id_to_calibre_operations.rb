class AddPipelineIdToCalibreOperations < ActiveRecord::Migration[6.0]
  def change
    add_reference :calibre_operations, :pipeline, null: false, foreign_key: true
  end
end
