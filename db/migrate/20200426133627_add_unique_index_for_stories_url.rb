class AddUniqueIndexForStoriesUrl < ActiveRecord::Migration[6.0]
  disable_ddl_transaction!

  def change
    add_index :stories, :url, unique: true, algorithm: :concurrently
  end
end
