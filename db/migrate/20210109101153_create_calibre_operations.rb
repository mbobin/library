class CreateCalibreOperations < ActiveRecord::Migration[6.0]
  def change
    create_table :calibre_operations do |t|
      t.integer :status, null: false, default: 0
      t.integer :op_type, null: false, default: 0
      t.jsonb :payload, null: false, default: {}
      t.jsonb :result, null: false, default: {}

      t.datetime :started_at
      t.datetime :finished_at
      t.timestamps
    end

    add_index :calibre_operations, :status
  end
end
