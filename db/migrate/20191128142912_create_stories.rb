class CreateStories < ActiveRecord::Migration[6.0]
  def change
    create_table :stories do |t|
      t.string :type
      t.string :url
      t.string :title
      t.text :description
      t.string :tags, null: false, array: true, default: []
      t.string :domain
      t.string :favicon_url
      t.string :image_url

      t.references :author, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
