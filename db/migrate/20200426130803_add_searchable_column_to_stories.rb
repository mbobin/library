class AddSearchableColumnToStories < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE OR REPLACE FUNCTION
      f_searchable_story(title text, tags text[], description text, domain text)
      RETURNS tsvector
      AS $CODE$
      BEGIN
          RETURN setweight(to_tsvector('english', coalesce(title, '')), 'A') ||
          setweight(to_tsvector('english', coalesce(tags::text, '')), 'A') ||
          setweight(to_tsvector('english', coalesce(description,'')), 'B') ||
          setweight(to_tsvector('english', coalesce(domain,'')), 'C');
      END
      $CODE$
      LANGUAGE plpgsql IMMUTABLE;
    SQL

    execute <<-SQL
      ALTER TABLE stories
      ADD COLUMN searchable tsvector GENERATED ALWAYS AS (f_searchable_story(title, tags, description, domain)) STORED;
    SQL
  end

  def down
    remove_column :stories, :searchable
  end
end
