class CreateTempDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :temp_documents do |t|
      t.references :pipeline, null: false, foreign_key: true

      t.timestamps
    end
  end
end
