class CreatePipelines < ActiveRecord::Migration[6.0]
  def change
    create_table :pipelines do |t|
      t.integer :current_step_idx, null: false, default: 0
      t.integer :operation_type, null: false, default: 0
      t.integer :status, null: false, default: 0

      t.jsonb :payload, null: false, default: {}

      t.timestamps
    end
  end
end
