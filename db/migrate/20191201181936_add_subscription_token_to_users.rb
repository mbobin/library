class AddSubscriptionTokenToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :subscription_token, :string
  end
end
