class RenameFaviconUrlColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column(:stories, :favicon_url, :website_icon_url)
  end
end
