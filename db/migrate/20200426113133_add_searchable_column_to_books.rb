class AddSearchableColumnToBooks < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE OR REPLACE FUNCTION
      f_searchable_book(name text, tags text[], description text)
      RETURNS tsvector
      AS $CODE$
      BEGIN
          RETURN setweight(to_tsvector('english', coalesce(name, '')), 'A') ||
          setweight(to_tsvector('english', coalesce(tags::text, '')), 'A') ||
          setweight(to_tsvector('english', coalesce(description,'')), 'B');
      END
      $CODE$
      LANGUAGE plpgsql IMMUTABLE;
    SQL

    execute <<-SQL
      ALTER TABLE books
      ADD COLUMN searchable tsvector GENERATED ALWAYS AS (f_searchable_book(name, tags, description)) STORED;
    SQL
  end

  def down
    remove_column :books, :searchable
  end
end
