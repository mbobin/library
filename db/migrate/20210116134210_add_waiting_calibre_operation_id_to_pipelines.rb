class AddWaitingCalibreOperationIdToPipelines < ActiveRecord::Migration[6.0]
  def change
    add_reference(:pipelines,
      :waiting_calibre_operation,
      foreign_key: {
        to_table: :calibre_operations,
        on_delete: :nullify
      }
    )
  end
end
