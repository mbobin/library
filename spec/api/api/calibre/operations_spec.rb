require 'rails_helper'

describe API::Calibre::Operations do
  include Rack::Test::Methods

  def app
    API::API
  end

  context 'POST /api/v1/calibre/operations/request' do
    let(:params) { {} }
    let(:operations_size) { 0 }

    before do
      create_list(:calibre_operation, operations_size)
      post '/api/v1/calibre/operations/request', params
    end

    context 'without operations' do
      it 'returns an empty array of operations' do
        expect(JSON.parse(last_response.body)).to eq []
      end

      it 'returns 200 OK' do
        expect(last_response.status).to eq(200)
      end
    end

    context 'with operations' do
      let(:operations_size) { 2 }

      it 'returns an array of operations' do
        expect(JSON.parse(last_response.body))
          .to match_array([
            a_hash_including('id', 'payload'),
            a_hash_including('id', 'payload')
          ])
      end

      it 'returns 200 OK' do
        expect(last_response.status).to eq(200)
      end

      it 'marks the operations as running' do
        expect(CalibreOperation.all.to_a).to all be_running
      end
    end

    context 'with limit' do
      let(:params) { { limit: 1 } }
      let(:operations_size) { 2 }

      it 'returns an array of operations' do
        expect(JSON.parse(last_response.body))
          .to match_array(a_hash_including('id', 'payload'))
      end

      it 'returns 200 OK' do
        expect(last_response.status).to eq(200)
      end
    end
  end

  context 'POST /api/v1/calibre/operations/:id/update' do
    let(:calibre_operation) { create(:calibre_operation) }

    before do
      post "/api/v1/calibre/operations/#{calibre_operation.id}/update", { status: :failed }
    end

    it 'returns an empty response' do
      expect(JSON.parse(last_response.body)).to eq({})
    end

    it 'returns 200 OK' do
      expect(last_response.status).to eq(200)
    end

    it 'updates the status' do
      expect(calibre_operation.reload).to be_failed
    end
  end
end
