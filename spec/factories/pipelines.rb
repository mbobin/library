FactoryBot.define do
  factory :pipeline do
    current_step_idx { 1 }
    operation_type { 1 }
    payload { "" }
  end
end
