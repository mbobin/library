FactoryBot.define do
  factory :calibre_operation do
    status { :pending }
    payload { { a: :b } }
    association :pipeline
  end
end
