namespace :import do
  desc "import files from location"
  task :files, [:path] => :environment do |_task, args|
    files_path = args[:path]
    unless files_path.present?
      puts "no files in this path"
      exit(1)
    end

    Dir.glob(Pathname(files_path).expand_path).each do |path|
      puts "Importing #{path}"
      Pipelines::Create.execute(path)
    end
  end
end
